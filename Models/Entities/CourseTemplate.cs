using System;

namespace Models
{
    public class CourseTemplate
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string CourseID { get; set; }
    }
}