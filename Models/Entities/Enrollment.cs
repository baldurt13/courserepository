namespace Models
{
    public class Enrollment
    {
        public int ID { get; set; }
        public string SSN { get; set; }
        public string CourseID { get; set; }
    }
}