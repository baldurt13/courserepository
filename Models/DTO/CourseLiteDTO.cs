using System;

namespace Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseLiteDTO
    {
        public int ID { get; set; }
        public string CourseID { get; set; }
        public string Semester { get; set; }
    }
}
