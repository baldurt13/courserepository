using System;
using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseDTO
    {
        public int ID { get; set; }
        public string CourseID { get; set; }
        public string Semester { get; set; }
        public string Name { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public List<Student> students { get; set; }
    }
}
