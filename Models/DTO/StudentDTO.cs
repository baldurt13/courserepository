namespace Models
{
    public class StudentDTO
    {
        public string Name { get; set; }
        public string SSN { get; set; }
    }
}