using System;
using System.Collections.Generic;

namespace Models
{
    /// <summary>
    /// 
    /// </summary>
    public class CourseViewModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }
}
