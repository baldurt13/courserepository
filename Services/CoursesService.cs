﻿using System;
using System.Linq;
using System.Collections.Generic;
using Models;


namespace Services
{   
    public class CoursesService : ICoursesService
    {
        private readonly AppDataContext _db;

        public CoursesService(AppDataContext db)
        {
            _db = db;
        }
        
        public List<CourseLiteDTO> GetCoursesBySemester(string semester)
        {            
            return (from x in _db.Courses
                    where x.Semester == semester
                    select new CourseLiteDTO
                    {
                        ID = x.ID,
                        CourseID = x.CourseID,
                        Semester = x.Semester,
                    }).ToList(); 
        }

        public CourseDTO GetCourseById(int id)
        {
            
            return  (from x in _db.Courses
                    join c in _db.CourseTemplates on x.CourseID equals c.CourseID
                    where x.ID == id
                    select new CourseDTO
                    {
                        ID = x.ID,
                        CourseID = x.CourseID,
                        Semester = x.Semester,
                        Name = c.Name,
                        StartDate = x.StartDate,
                        EndDate = x.EndDate,
                        students = null
                    }).SingleOrDefault();
        }

        public CourseDTO UpdateCourseById(int id, CourseViewModel model)
        {
            var course = (from c in _db.Courses
                        where c.ID == id
                        select c).SingleOrDefault();

            if (course == null) 
                return null;

            course.StartDate = model.StartDate;
            course.EndDate = model.EndDate;

            _db.Update(course);
            _db.SaveChanges();

            return new CourseDTO
            {
                ID = course.ID,
                CourseID = course.CourseID,
                Semester = course.Semester,
                Name = (from d in _db.Courses
                        join c in _db.CourseTemplates on d.CourseID equals c.CourseID
                        where d.ID == id
                        select c).SingleOrDefault().Name,
                StartDate = course.StartDate,
                EndDate = course.EndDate,
                students = null
            };
        }

        public bool DeleteCourseById(int courseId)
        {
            var course = (from c in _db.Courses
                            where c.ID == courseId
                            select c).SingleOrDefault();
            
            if (course == null)
                return false;

            _db.Remove(course);
            _db.SaveChanges();

            return true;
        }

        private CourseDTO GetCourseDTOById(int id)
        {
            var course = (from c in _db.Courses
                        where c.ID == id
                        select c).SingleOrDefault();

            if (course == null)
            {
                return null;
            }

            return new CourseDTO
            {
                ID = course.ID,
                CourseID = course.CourseID,
                Semester = course.Semester,
                Name = (from d in _db.Courses
                        join c in _db.CourseTemplates on d.CourseID equals c.CourseID
                        where d.ID == id
                        select c).SingleOrDefault().Name,
                StartDate = course.StartDate,
                EndDate = course.EndDate,
                students = null
            };
        }

        public List<StudentDTO> GetStudentsByCourseId(int id)
        {
            var students = (from e in _db.Enrollments
                            join ct in _db.CourseTemplates on e.CourseID equals ct.CourseID
                            join s in _db.Students on e.SSN equals s.SSN
                            where ct.ID == id
                            select new StudentDTO
                            {
                                Name = s.Name,
                                SSN = s.SSN
                            }).ToList();
                            
            return students;
        }
    }
}
