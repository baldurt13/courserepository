using System;
using System.Collections.Generic;
using Models;

namespace Services
{
    public interface ICoursesService
    {
        List<CourseLiteDTO> GetCoursesBySemester(string semester);

        CourseDTO GetCourseById(int id);

        CourseDTO UpdateCourseById(int id, CourseViewModel model);

        bool DeleteCourseById(int courseId);

        List<StudentDTO> GetStudentsByCourseId(int courseId);
    }
    
}
