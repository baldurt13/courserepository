using System;
using System.Collections.Generic;
using Models;

namespace Services
{
    public interface IStudentsService
    {
        List<StudentDTO> GetStudentsByCourseId(int courseId);
    }
    
}
