using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using Models;
using Services;

namespace WebApplication.Controllers
{
    [Route("api/courses")]
    public class CoursesController : Controller
    {
        private readonly ICoursesService _service;

        public CoursesController(ICoursesService service)
        {
            _service = service;
        }
        
        [HttpGet]
        public List<CourseLiteDTO> GetCoursesOnSemester(string semester="20163") 
        {
            return _service.GetCoursesBySemester(semester);
        }

        [HttpGet]
        [Route("{id:int}/", Name="GetCourseById")]
        public IActionResult GetCourseById(int id)
        {
            var course = _service.GetCourseById(id);

            if (course == null)
            {
                return NotFound();
            }
            
            return new ObjectResult(course);
        }

        [HttpPut]
        [Route("{id:int}")]
        public IActionResult UpdateCourseById(int id, [FromBody]CourseViewModel model)
        {
            CourseDTO course = _service.UpdateCourseById(id, model);

            if (course == null) 
            {
                return NotFound();
            }

            return new NoContentResult();
        }

        [HttpDelete]
        [Route("{id:int}/")]
        public IActionResult DeleteCourse(int courseId)
        {
            if (_service.DeleteCourseById(courseId))
            {
                return Ok();
            }

            return NotFound();
        }

        [HttpGet]
        [Route("{id:int}/students/")]
        public List<StudentDTO> GetStudentsInCourse(int courseId)
        {   
            return _service.GetStudentsByCourseId(courseId);
        }
        
    }
}